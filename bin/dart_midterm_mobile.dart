import 'package:dart_midterm_mobile/dart_midterm_mobile.dart'
    as dart_midterm_mobile;
import 'dart:io';

void main() {
  //var token;

  print("input textnum :  ");
  String txtnum = stdin.readLineSync()!;
  text(txtnum);
}

void text(String txtnum) {
  String ospaces;
  ospaces = txtnum.replaceAll(" ", "");
  //print(ospaces);
  var token = ospaces.split("");
  print(token);
  token1(token);
}

void token1(List token) {
  var operators = new List<String>.filled(0, "", growable: true);
  var postfix = new List<String>.filled(0, "", growable: true);
  for (int i = 0; i < token.length; i++) {
    if (token[i] == "1" || token[i] == "-1") {
      postfix.add(token[i]);
    } else if (token[i] == "2" || token[i] == "-2") {
      postfix.add(token[i]);
    } else if (token[i] == "3" || token[i] == "-3") {
      postfix.add(token[i]);
    } else if (token[i] == "4" || token[i] == "-4") {
      postfix.add(token[i]);
    } else if (token[i] == "5" || token[i] == "-5") {
      postfix.add(token[i]);
    } else if (token[i] == "6" || token[i] == "-6") {
      postfix.add(token[i]);
    } else if (token[i] == "7" || token[i] == "-7") {
      postfix.add(token[i]);
    } else if (token[i] == "8" || token[i] == "-8") {
      postfix.add(token[i]);
    } else if (token[i] == "9" || token[i] == "-9") {
      postfix.add(token[i]);
    } else if (token[i] == "0") {
      postfix.add(token[i]);
    }
    if (token[i].compareTo('+') == 0) {
      while (operators.isNotEmpty &&
          operators.last.compareTo("(") != 0 &&
          precedence(token[i]) < precedence(operators.last)) {
        postfix.add(operators.last);
        operators.removeLast();
      }
      operators.add(token[i]);
    } else if (token[i].compareTo('-') == 0) {
      while (operators.isNotEmpty &&
          operators.last.compareTo("(") != 0 &&
          precedence(token[i]) < precedence(operators.last)) {
        postfix.add(operators.last);
        operators.removeLast();
      }
      operators.add(token[i]);
    } else if (token[i].compareTo('*') == 0) {
      while (operators.isNotEmpty &&
          operators.last.compareTo("(") != 0 &&
          precedence(token[i]) < precedence(operators.last)) {
        postfix.add(operators.last);
        operators.removeLast();
      }
      operators.add(token[i]);
    } else if (token[i].compareTo('/') == 0) {
      while (operators.isNotEmpty &&
          operators.last.compareTo("(") != 0 &&
          precedence(token[i]) < precedence(operators.last)) {
        postfix.add(operators.last);
        operators.removeLast();
      }
      operators.add(token[i]);
    } else if (token[i].compareTo('^') == 0) {
      while (operators.isNotEmpty &&
          operators.last.compareTo("(") != 0 &&
          precedence(token[i]) < precedence(operators.last)) {
        postfix.add(operators.last);
        operators.removeLast();
      }
      operators.add(token[i]);
    } else if (token[i].compareTo('%') == 0) {
      while (operators.isNotEmpty &&
          operators.last.compareTo("(") != 0 &&
          precedence(token[i]) < precedence(operators.last)) {
        postfix.add(operators.last);
        operators.removeLast();
      }
      operators.add(token[i]);
    }
    if (token[i].compareTo("(") == 0) {
      operators.add(token[i]);
    }
    if (token[i].compareTo(")") == 0) {
      while (operators.last.compareTo("(") != 0) {
        postfix.add(operators.last);
        operators.removeLast();
      }
      operators.removeLast();
    }
  }
  while (operators.isNotEmpty) {
    postfix.add(operators.last);
    operators.removeLast();
  }
  print(postfix);
  token2(postfix);
}

void token2(List token) {
  var values = new List<int>.filled(0, 0, growable: true);
  var num;
  for (int i = 0; i < token.length; i++) {
  if (token[i] == "1" || token[i] == "-1") {
      num=int.parse(token[i]);
      values.add(num);
    } else if (token[i] == "2" || token[i] == "-2") {
      num=int.parse(token[i]);
      values.add(num);
    } else if (token[i] == "3" || token[i] == "-3") {
      num=int.parse(token[i]);
      values.add(num);
    } else if (token[i] == "4" || token[i] == "-4") {
      num=int.parse(token[i]);
      values.add(num);
    } else if (token[i] == "5" || token[i] == "-5") {
      num=int.parse(token[i]);
      values.add(num);
    } else if (token[i] == "6" || token[i] == "-6") {
      num=int.parse(token[i]);
      values.add(num);
    } else if (token[i] == "7" || token[i] == "-7") {
      num=int.parse(token[i]);
      values.add(num);
    } else if (token[i] == "8" || token[i] == "-8") {
      num=int.parse(token[i]);
      values.add(num);
    } else if (token[i] == "9" || token[i] == "-9") {
      num=int.parse(token[i]);
      values.add(num);
    } else if (token[i] == "0") {
      num=int.parse(token[i]);
      values.add(num);
    }
    else{
      var right=values.removeLast();
      var left=values.removeLast();
      var sum=1;
      if (token[i].compareTo('+') == 0) {
        sum=left + right;
        values.add(sum);
      }
      else if (token[i].compareTo('-') == 0) {
        sum=left - right;
        values.add(sum);
      }
      else if (token[i].compareTo('*') == 0) {
        sum=left * right;
        values.add(sum);
      }
      else if (token[i].compareTo('/') == 0) {
        sum=left ~/ right;
        values.add(sum);
      }
      else if (token[i].compareTo('^') == 0) {
        for(i=0;i<right;i++){
          sum=sum*left;
        }
        values.add(sum);
      }
      else if (token[i].compareTo('%') == 0) {
        sum=left % right;
        values.add(sum);
      }
      print(values.first);
      
    }
  }
}

int precedence(String c) {
  switch (c) {
    case '+':
    case '-':
      return 1;

    case '*':
    case '/':
      return 2;

    case '^':
      return 3;
  }
  return -1;
}
